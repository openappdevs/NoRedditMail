package org.openappdevs.noredditmail;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class HandlerActivity extends AppCompatActivity {

    private static String TAG = "NoRedditMail";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        Uri data = intent.getData();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String action = prefs.getString("action","open");

        if(intent.getAction().equals(Intent.ACTION_VIEW) && data != null) {
            String iterationOne = data.getPath().replace(data.getPath().substring(data.getPath().indexOf("?")),"");
            String iterationTwo = iterationOne.replace("/CL0/","");

            if(action.equals("open")) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(iterationTwo));
                PackageManager packageManager = getPackageManager();
                if (browserIntent.resolveActivity(packageManager) != null) {
                    startActivity(browserIntent);
                } else {
                    Log.d(TAG, "No Intent available to handle action");
                    Toast.makeText(this, R.string.cannot_open, Toast.LENGTH_LONG).show();
                }
            }else {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, iterationTwo);
                sendIntent.setType("text/plain");

                Intent shareIntent = Intent.createChooser(sendIntent, null);
                startActivity(shareIntent);
            }
        }

        finish();
        setContentView(R.layout.activity_handler);
    }
}